

/*
 * POSIX Real Time Example
 * using a single pthread as RT thread
 */

#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>

void *
thread_func (void *data)
{
  /* Do RT specific stuff here */
  return NULL;
}

int
main (int argc, char *argv[])
{
  struct sched_param param;
  void *stack_buf;
  pthread_t thread;
  pthread_attr_t attr;
  int ret;

  /* Lock memory */
  if (mlockall (MCL_CURRENT | MCL_FUTURE) == -1)
    {
      printf ("mlockall failed: %m\n");
      exit (-2);
    }

  /* Pre-fault stack for the thread */
  stack_buf = mmap (NULL, PTHREAD_STACK_MIN, PROT_READ | PROT_WRITE,
                    MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
  if (stack_buf == MAP_FAILED)
    {
      printf ("mmap failed: %m\n");
      exit (-1);
    }
  memset (stack_buf, 0, PTHREAD_STACK_MIN);

  /* Initialize pthread attributes (default values) */
  ret = pthread_attr_init (&attr);
  if (ret)
    {
      printf ("init pthread attributes failed\n");
      goto out;
    }

  /* Set pthread stack to already pre-faulted stack */
  ret = pthread_attr_setstack (&attr, stack_buf, PTHREAD_STACK_MIN);
  if (ret)
    {
      printf ("pthread setstack failed\n");
      goto out;
    }

  /* Set scheduler policy and priority of pthread */
  ret = pthread_attr_setschedpolicy (&attr, SCHED_FIFO);
  if (ret)
    {
      printf ("pthread setschedpolicy failed\n");
      goto out;
    }
  param.sched_priority = 80;
  ret = pthread_attr_setschedparam (&attr, &param);
  if (ret)
    {
      printf ("pthread setschedparam failed\n");
      goto out;
    }
  /* Use scheduling parameters of attr */
  ret = pthread_attr_setinheritsched (&attr, PTHREAD_EXPLICIT_SCHED);
  if (ret)
    {
      printf ("pthread setinheritsched failed\n");
      goto out;
    }

  /* Create a pthread with specified attributes */
  ret = pthread_create (&thread, &attr, thread_func, NULL);
  if (ret)
    {
      printf ("create pthread failed\n");
      goto out;
    }

  /* Join the thread and wait until it is done */
  ret = pthread_join (thread, NULL);
  if (ret)
    printf ("join pthread failed: %m\n");

out:
  munmap (stack_buf, PTHREAD_STACK_MIN);
  return ret;
}
